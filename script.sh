#!/bin/bash

# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

set -ex

/bin/sleep 30&
sleep 2 # wait for fork to exec
kill -SEGV $!

ls -lah /bin/sleep
ls -lah /tmp/

for c in /tmp/core.*; do
  # /tmp/core.%E.%p.%s.%t
  parts=(${c//./ })
  exe=${parts[1]}
  exe=(${exe//!/\/})
  gdb --batch --eval-command bt --core "$c" "$exe"
done
